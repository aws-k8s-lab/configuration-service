FROM maven:3.9.6-amazoncorretto-17 AS build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTests

FROM openjdk:17-alpine
WORKDIR /app
COPY --from=build /app/target/configuration-service-0.0.1-SNAPSHOT.jar .
CMD ["java", "-jar", "my-application.jar"]